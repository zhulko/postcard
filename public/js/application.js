$(function() {

    function var_dump(obj) {
        var out = '';
        for (var i in obj) {
            out += i + ": " + obj[i] + "\n";
        }
        var pre = document.createElement('pre');
        pre.innerHTML = out;
        document.body.appendChild(pre)
    }


    $('form.prevent_auto_submit input,form.prevent_auto_submit select').keypress(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            $(this).trigger('change');
        }
    });

    function validateForm(form) {
        // need to add form validation
        return true;
    }



    $('#message_sent').on('click', '.dismiss' , function() {
        console.log('fff');
        $('#message_sent').hide();
        $('#message').show();
    });


    $('#send_message').click(function (e) {
        var valid = validateForm($('#upload_image_form'));
        var form_data = new FormData($('#message_form').get(0));
        if (valid === true) {
            $.ajax({
                url: url + 'messages/sendmessage',
                type: 'POST',
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data)
                {
                    $('#message').hide();
                    $('#message_sent').show();
                    $('#message_sent').html(data);
                }
            });
        }
    });


    function throttle(func, wait) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            if (!timeout) {
                // the first time the event fires, we setup a timer, which
                // is used as a guard to block subsequent calls; once the
                // timer's handler fires, we reset it and create a new one
                timeout = setTimeout(function() {
                    timeout = null;
                    func.apply(context, args);
                }, wait);
            }
        }
    }


    var MAX_AJAX_REQUESTS = 1;
    var COUNT_AJAX_REQUESTS = 0;
    var AJAX_DATA_SENT = '';

    function prepareToSavePostcardText() {
        if( $('#postcard_data').length ) {
            console.log('prepare');
            COUNT_AJAX_REQUESTS = 0;
            $('.postcard_text').each(function (index, currentElement) {
                var top = $(currentElement).css('margin-top');
                var left = $(currentElement).css('margin-left');
                var json_fields_string = '{"' +
                    'postcard-text-coord_' + index + '":"' + parseInt(top) + ',' + parseInt(left) + '","' +
                    'postcard-text_' + index + '":"' + encodeURI($(currentElement).text()) + '","' +
                    'postcard-text-width_' + index + '":"' + parseInt($(currentElement).width()) + '","' +
                    'postcard-text-height_' + index + '":"' + parseInt($(currentElement).height()) + '","' +
                    'postcard-image-width_' + index + '":"' + parseInt($('.postcard_image').width()) + '","' +
                    'postcard-image-height_' + index + '":"' + parseInt($('.postcard_image').height()) + '","' +
                    'postcard-text-size_' + index + '":"' + parseInt($(currentElement).css('font-size')) +
                    '"}';

                var fields_data = JSON.parse(json_fields_string);

                console.log(fields_data);

                $.each(fields_data, function (field, field_val) {
                    if ($('#' + field).length === 0) {
                        $('<input>').attr({
                            type: 'hidden',
                            id: field,
                            name: field,
                            value: field_val
                        }).appendTo('#postcard_data');
                    } else {
                        $('#' + field).val(field_val);
                    }
                });
            });

            throttle(function () {
                savePostcardText();
            }, 1000)();
        }
    }


    function savePostcardText() {
        var form_data_to_check = $('#postcard_data').serialize();
        if(COUNT_AJAX_REQUESTS < MAX_AJAX_REQUESTS && (form_data_to_check !== AJAX_DATA_SENT)) {
            AJAX_DATA_SENT = $('#postcard_data').serialize();
            COUNT_AJAX_REQUESTS++;
            console.log('sent');
            $.ajax({
                url: url + 'home/savePostcardData',
                type: 'POST',
                data: new FormData($('#postcard_data').get(0)),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    //$('#ajax_response').prepend(data);
                    //$('#ajax_response_panel').show('slow');
                }
            });
        }
    }

    /*
     * draggable text on postcard
     *
     */

    function editPostcardText(current_edit_button) {
        if (!current_edit_button.parents('.not_draggable').length) {
            current_edit_button.children('.fa').removeClass('fa-pencil');
            current_edit_button.children('.fa').addClass('fa-check');
            current_edit_button.parent().addClass('not_draggable');
        } else {
            current_edit_button.children('.fa').removeClass('fa-check');
            current_edit_button.children('.fa').addClass('fa-pencil');
            current_edit_button.parent().removeClass('not_draggable');
        }
    }


    $('.ajax_response_panel_close').click(function (e) {
        $('#ajax_response').empty();
        $('#ajax_response_panel').hide('slow');
    });


    $('.tab_icon span').hide();
    $('.tab_icon').on('mouseover', function () {
        $(this).children('span').show();
    }).on('mouseout', function () {
        $(this).children('span').hide();
    });


    var edit_buttons =
        `
        <div class="edit_button" id="edit_button" draggable="false" contenteditable="false"><i class="fa fa-pencil" aria-hidden="true"></i></div>`;

    var dragging = false,
        currentDragged;

    var resize_handles =
        `
<div class="resize nw" id="nw" draggable="false" contenteditable="false"></div>
<div class="resize n" id="n" draggable="false" contenteditable="false"></div>
<div class="resize ne" id="ne" draggable="false" contenteditable="false"></div>
<div class="resize w" id="w" draggable="false" contenteditable="false"></div>
<div class="resize e" id="e" draggable="false" contenteditable="false"></div>
<div class="resize sw" id="sw" draggable="false" contenteditable="false"></div>
<div class="resize s" id="s" draggable="false" contenteditable="false"></div>
<div class="resize se" id="se" draggable="false" contenteditable="false"></div>`;

    var resizing = false,
        current_resize_handle,
        sX,
        sY;

    var mousedownEventType = ((document.ontouchstart!==null)?'mousedown':'touchstart'),
        mousemoveEventType = ((document.ontouchmove!==null)?'mousemove':'touchmove'),
        mouseupEventType = ((document.ontouchmove!==null)?'mouseup':'touchend');



    $('.draggable')
        .on(mousedownEventType, function(e){
            if (!e.target.classList.contains('resize') && !resizing) {
                if (!$(this).hasClass('not_draggable')) {
                    currentDragged = $(this);
                    dragging = true;
                    sX = e.pageX;
                    sY = e.pageY;
                }
            }
        });

    $('.resizable')
        .focus(function(e){
            $(this).html($(this).html() + edit_buttons + resize_handles);
            $('.resize').on(mousedownEventType, function(e){
                current_resize_handle = $(this);
                resizing = true;
                sX = e.pageX;
                sY = e.pageY;
            });
            $('.edit_button').on(mousedownEventType, function(e){
                editPostcardText($(this));
            });
        })
        .blur(function(e){
            $(this).children('.resize').remove();
            $(this).children('.edit_button').remove();
            $(this).children('.fa').removeClass('fa-check');
            $(this).children('.fa').addClass('fa-pencil');
            $(this).removeClass('not_draggable');
            prepareToSavePostcardText();
        });



    $("body").on(mousemoveEventType, function(e) {
        var xChange = e.pageX - sX,
            yChange = e.pageY - sY;
        if (resizing) {
            e.preventDefault();

            var parent  = current_resize_handle.parent();

            switch (current_resize_handle.attr('id')) {
                case "nw":
                    var newWidth = parseFloat(parent.css('width')) - xChange,
                        newHeight = parseFloat(parent.css('height')) - yChange,
                        newLeft = parseFloat(parent.css('margin-left')) + xChange,
                        newTop = parseFloat(parent.css('margin-top')) + yChange;
                    break;
                case "n":
                    var newWidth = parseFloat(parent.css('width')),
                        newHeight = parseFloat(parent.css('height')) - yChange,
                        newLeft = parseFloat(parent.css('margin-left')),
                        newTop = parseFloat(parent.css('margin-top')) + yChange;
                    break;
                case "ne":
                    var newWidth = parseFloat(parent.css('width')) + xChange,
                        newHeight = parseFloat(parent.css('height')) - yChange,
                        newLeft = parseFloat(parent.css('margin-left')),
                        newTop = parseFloat(parent.css('margin-top')) + yChange;
                    break;
                case "e":
                    var newWidth = parseFloat(parent.css('width')) + xChange,
                        newHeight = parseFloat(parent.css('height')),
                        newLeft = parseFloat(parent.css('margin-left')),
                        newTop = parseFloat(parent.css('margin-top'));
                    break;
                case "w":
                    var newWidth = parseFloat(parent.css('width')) - xChange,
                        newHeight = parseFloat(parent.css('height')),
                        newLeft = parseFloat(parent.css('margin-left')) + xChange,
                        newTop = parseFloat(parent.css('margin-top'));
                    break;
                case "sw":
                    var newWidth = parseFloat(parent.css('width')) - xChange,
                        newHeight = parseFloat(parent.css('height')) + yChange,
                        newLeft = parseFloat(parent.css('margin-left')) + xChange,
                        newTop = parseFloat(parent.css('margin-top'));
                    break;
                case "s":
                    var newWidth = parseFloat(parent.css('width')),
                        newHeight = parseFloat(parent.css('height')) + yChange,
                        newLeft = parseFloat(parent.css('margin-left')),
                        newTop = parseFloat(parent.css('margin-top'));
                    break;
                case "se":
                    var newWidth = parseFloat(parent.css('width')) + xChange,
                        newHeight = parseFloat(parent.css('height')) + yChange,
                        newLeft = parseFloat(parent.css('margin-left')),
                        newTop = parseFloat(parent.css('margin-top'));
                    break;
            }
            //Width
            var containerWidth = parseFloat(parent.parent().css("width"));

            if (newLeft < 0) {
                newWidth += newLeft;
                newLeft = 0;
            }
            if (newWidth < 0) {
                newWidth = 0;
                newLeft = parent.css("margin-left");
            }
            if (newLeft + newWidth > containerWidth) {
                newWidth = containerWidth-newLeft;
            }

            parent
                .css('margin-left', newLeft + "px")
                .css('width', newWidth + "px");
            sX = e.pageX;

            //Height
            var containerHeight = parseFloat(parent.parent().css("height"));

            if (newTop < 0) {
                newHeight += newTop;
                newTop = 0;
            }
            if (newHeight < 0) {
                newHeight = 0;
                newTop = parent.css("margin-top");
            }
            if (newTop + newHeight > containerHeight) {
                newHeight = containerHeight-newTop;
            }

            parent
                .css('margin-top', newTop + "px")
                .css('height', newHeight + "px");
            sY = e.pageY;

        } else if (dragging) {
            e.preventDefault();

            var draggedWidth = parseFloat(currentDragged.css("width")),
                draggedHeight = parseFloat(currentDragged.css("height")),
                containerWidth = parseFloat(currentDragged.parent().css("width")),
                containerHeight = parseFloat(currentDragged.parent().css("height"));

            var newLeft = (parseFloat(currentDragged.css("margin-left")) + xChange),
                newTop = (parseFloat(currentDragged.css("margin-top")) + yChange);

            if (newLeft < 0) {
                newLeft = 0;
            }
            if (newTop < 0) {
                newTop = 0;
            }
            if (newLeft + draggedWidth > containerWidth) {
                newLeft = containerWidth - draggedWidth;
            }
            if (newTop + draggedHeight > containerHeight) {
                newTop = containerHeight - draggedHeight;
            }

            currentDragged
                .css("margin-left", newLeft + "px")
                .css("margin-top", newTop + "px");

            sX = e.pageX;
            sY = e.pageY;

        }
    }).on(mouseupEventType, function(e){
            dragging = false;
            resizing = false;
            prepareToSavePostcardText();
        });





    /*
     * form image upload test
     *
     */


    $('#upload_image').click(function (e) {
        var valid = validateForm($('#upload_image_form'));
        $('#img_source').val('upload');
        setTimeout(function () {
            var form_data = new FormData($('#upload_image_form').get(0));
            if (valid === true) {
                $.ajax({
                    url: url + 'images/addimage',
                    type: 'POST',
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data)
                    {

                    }
                });
            } else {

            }
        }, 500);
    });


    function basename(str) {
        return str.split(/\.[^.]+$/)[0];
    }



    /*
     * list of uploaded images
     *
     */
    function getUploadedImages() {
        $.ajax({
            url: url + 'images/getUploadedImages',
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            success: function (data)
            {
                $('#uploaded_images').html(data);
            }
        });
    }



    /*
     * drop upload images
     *
     */


    var dropArea = $("#drop_area");
    $(dropArea)
        .bind( "dragenter dragover dragleave drop", function(e) {
            e.preventDefault();
            e.stopPropagation();
        })
        .bind( "dragenter dragover", function(e) {
            $(dropArea).addClass('highlight');
        })
        .bind( "dragleave drop", function(e) {
            $(dropArea).removeClass('highlight');
        })
        .bind( "drop", function(e) {
            var dt = e.originalEvent.dataTransfer;
            var files = dt.files;
            handleFiles(files);
        });

    $('#file_to_upload').bind('change', function (e) {
        handleFiles($(this).get(0).files);
    });

    var uploadProgress = [];
    var progressBar = $('#progress_bar');

    function initializeProgress(numFiles) {
        $(progressBar).show();
        $(progressBar).val(0);
        uploadProgress = [];

        for(var i = numFiles; i > 0; i--) {
            uploadProgress.push(0);
        }
    }

    function updateProgress(fileNumber, percent) {
        uploadProgress[fileNumber] = percent;
        var total = uploadProgress.reduce((tot, curr) => tot + curr, 0) / uploadProgress.length;
        $(progressBar).val(total);
    }

    function handleFiles(files) {
        $('.info').hide();
        var file_names = [];
        for (var i = 0; i < files.length; ++i) {
            file_names.push(files[i].name);
        }
        var files_arr = [];
        for (var i = 0; i < files.length; ++i) {
            files_arr.push(files[i]);
        }
        initializeProgress(files_arr.length);
        //files_arr.forEach(uploadFile);
        files_arr.forEach(previewFile);
    }

    function previewFile(file, i) {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = function() {
            if (file.size > MAXFILE_SIZE) { 
                $('#list_uploading_images').append('<li id="F'+basename(file.name)+'" class="file_error"><img src="'+reader.result+'"/><span>This file is too big</span></li>');
            } else {
                $('#list_uploading_images').append('<li id="F'+basename(file.name)+'"><img src="'+reader.result+'"/><span>'+file.name+'</span></li>');
                uploadFile(file, i);
            }
        }
    }

    function uploadFile(file, i) {
        var url = '/images/addImage';
        var xhr = new XMLHttpRequest();
        $(progressBar).show();
        console.log(file);
        

        xhr.open('POST', url, true);
        xhr.setRequestHeader("Content-Type", file.type);
        xhr.setRequestHeader("Cache-Control", "no-cache");
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.setRequestHeader("X-File-Name", file.name);
        xhr.setRequestHeader("X-File-Size", file.size);
        xhr.setRequestHeader("X-File-Type", file.type);

        xhr.upload.addEventListener('loadstart', onloadstartHandler, false);
        xhr.upload.addEventListener('load', onloadHandler, false);

        // Update progress (can be used to show progress indicator)
        xhr.upload.addEventListener("progress", function(e) {
            updateProgress(i, (e.loaded * 100.0 / e.total) || 100);
            var percent = e.loaded/e.total*100;
            //$('#ajax_response').prepend('<' + 'br>Progress' + percent + '%');
        });

        xhr.addEventListener('readystatechange', function(e) {
            if (xhr.readyState == 4 && xhr.status == 200) {
                updateProgress(i, 100); // <- Add this
                if (xhr.responseText.toLowerCase().indexOf("error") >= 0) {
                    $('#ajax_response').html('<pre>' + xhr.responseText + '</pre>');
                    $('#ajax_response_panel').show();
                }
                getUploadedImages();
            }
            else if (xhr.readyState == 4 && xhr.status != 200) {
                // Error.
                console.log('Upload Error');
                $('#ajax_response').prepend('Upload Error');
                $('#ajax_response_panel').show();
            }
        });

        //$('#ajax_response_panel').show();
        xhr.send(file);
    }

    // Handle the start of the transmission
    function onloadstartHandler(evt) {
        //$('#ajax_response').prepend('<' + 'br>Upload started.');
    }
    // Handle the end of the transmission
    function onloadHandler(evt) {
        //$('#ajax_response').prepend('<' + 'br>File uploaded. Waiting for response.');

    }



});



