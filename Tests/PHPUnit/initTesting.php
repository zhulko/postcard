<?php
//only show errors - remove the notices and warning
error_reporting(E_ALL  & ~E_NOTICE & ~E_WARNING);

//uncomment to override application environment
define('TEST_ENVIRONMENT', 'unittesting');

// TEST_PATH should be /Test
define("TEST_PATH", substr(dirname(__FILE__), 0, -8));

defined('APPLICATION_PATH')
		|| define('APPLICATION_PATH', TEST_PATH . '/../application');

defined('ROOT')
		|| define('ROOT', TEST_PATH . '/../public');

//$ignoreDbTearDown = true;

if(!isset($ignoreDbTearDown)){

	//include code to tear down and build database
	require_once(TEST_PATH.'/Database/scripts/initDb.php');

	//drop all the tables
	dropDbTables();

	//create the tables
    $result = createDbTales();

	//insert the default data
    if($result){
        insertTableData();
    }

}


//access domain name (site specific settings)
$_SERVER['SERVER_NAME'] = '192.168.33.66';

//set default server location timezone
date_default_timezone_set("America/New_York");


require_once "TestInputData.php";
//the app may override this, so we have to set it again
error_reporting(E_ALL  & ~E_NOTICE & ~E_WARNING);


