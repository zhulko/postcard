<?php
/*
 * -- this is not done
 * Run from /var/www/html/postcard/Tests as
 * phpunit
 *
 */


use PHPUnit\Framework\TestCase;
use PCApp\Model\Image;
//$ignoreDbTearDown = true;



//does the app includes and sets the testing environment
require_once(dirname(__FILE__) . '/../initTesting.php');


class ImageTest extends TestCase {

	protected $service;

	/**
	 * Prepares the environment before running a test.
	 */
	protected function setUp() {
		//parent::setUp ();
        require_once(APPLICATION_PATH . '/Core/Model.php');
        require_once(APPLICATION_PATH . '/Model/Image.php');

		$this->service = new Image();
	}

	/**
	 * Test Service Exists
	 */
	public function testServiceConstruct(){
		$this->assertNotNull($this->service);
	}

	/**
	 *
	 * @depends testServiceConstruct
	 */
	public function testGetImage(){
		$formData = TestInputData::IMAGE_ID;
        $finalObj = $this->service->getImage($formData);
        $this->assertNotNull($finalObj);
        $this->assertEquals(1, count($finalObj));
	}


	/**
	 *
	 * Test Add New image
	 */
	public function testAddNewImage()
	{
		$formData = TestInputData::getImageData();
        $formData['img_filename'] = base64_encode(random_bytes(25));
        $insertData = $this->service->addImage($formData);

        $this->assertNotNull($insertData);

        $lastInsertId = $insertData;
        $this->assertNotNull($lastInsertId);

        $returnData = $this->service->getImage($lastInsertId);
        $this->assertEquals($lastInsertId, $returnData->img_id);
	}


	/**
	 *
	 * Test Add New Image img_filename Empty
     * * -- this is not done
	 */
	public function testFilenameEmpty()
	{
		$formData = TestInputData::getImageData();
		$formData['img_filename'] = '';
		list($finalObj, $errors) = $this->service->addImage($formData);
		$this->assertNotNull($errors);
		$this->assertGreaterThan(0, count($errors));
		$this->assertNull($finalObj);
		$this->assertEquals(0, count($finalObj));
	}


	/**
	 *
	 * Test New Image file missing
     * * -- this is not done
	 */
	/*
	public function testNewImageFileMissing()
	{

	}
    */


	/**
	 * Test New Image already exists
     * * -- this is not done
	 */
	public function testNewImageAlreadyExists()
	{
        $formData = TestInputData::getImageData();
        list($finalObj, $errors) = $this->service->addImage($formData);
        $this->assertNotNull($errors);
        $this->assertGreaterThan(0, count($errors));
        $this->assertNull($finalObj);
        $this->assertEquals(0, count($finalObj));
	}



	/**
	 * Cleans up the environment after running each test method.
	 */
	protected function tearDown() {
		// TODO Auto-generated TestService_Staff::tearDown()
		parent::tearDown ();
	}
}
