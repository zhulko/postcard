<?php
/**
 * This used used as a input parameters for test methods
 */
class TestInputData {

	/*	 *
	 * The variables below this section are currently valid.
	 * The ID's match the default data records found in /Test/Database/dbv/data/default/
	 * that is used in the unit testing database.
	 *
	 */
	const USER_ID = 6;
	const POSTCARD_ID = 2;
	const MESSAGE_ID = 9;
    const MESSAGE_EMAIL = 'vzhulkovsky@gmail.com';
    const IMAGE_ID = 36;
    const IMAGE_FILENAME = '1679091c5a880faf6fb5e6087eb1b2dc_6b4e9008fee3569a52f254885be100ec.jpg';


	public static function getLoginUserFormData(){


	}

	public static function getPostcardData(){

		return array(
			'p_id'=>self::POSTCARD_ID,
			'user_id'=>self::USER_ID,
			'p_text'=>mt_rand().'PHPUnit Test',
			'image_id'=>self::IMAGE_ID,
			'img_exif'=>base64_encode(random_bytes(100)),
			'p_created'=>date("Y-m-d H:i:s"),
            'p_modified'=>date("Y-m-d H:i:s")
		);
	}

	public static function getMessageData(){

		return array(
			'message_id'=>self::MESSAGE_ID,
			'message_content'=>mt_rand().'PHPUnit Test',
			'message_email'=>self::MESSAGE_EMAIL,
            'p_id'=>self::POSTCARD_ID,
			'user_id'=>self::USER_ID,
            'message_created'=>date("Y-m-d H:i:s"),
            'message_sent'=>date("Y-m-d H:i:s")
		);
	}

  public static function getImageData(){
		return array(
			'img_id'=>self::IMAGE_ID,
            'user_id'=>self::USER_ID,
			'img_filename'=>self::IMAGE_FILENAME,
            'img_created'=>date("Y-m-d H:i:s"),
            'img_modified'=>date("Y-m-d H:i:s")
		);
	}


    public static function getImageIds(){
		return array( 36, 39, 41 );
	}
     public static function getPostcardIds(){
		return array(  2, 6, 11  );
	}
    public static function getMessageIds(){
		return array( 9   );
	}


}
