CREATE TABLE IF NOT EXISTS `messages` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message_email` varchar(256) DEFAULT NULL,
  `message_subject` varchar(256) DEFAULT NULL,
  `message_content` text,
  `message_created` datetime NOT NULL,
  `message_sent` datetime DEFAULT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;