CREATE TABLE IF NOT EXISTS `images` (
  `img_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `img_filename` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `file_modified` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_created` datetime NOT NULL,
  `img_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `img_exif` text COLLATE utf8_unicode_ci,
  `img_source` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_title` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`img_id`),
  UNIQUE KEY `img_id` (`img_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=46 ;
