<?php

if(!defined('TEST_ENVIRONMENT')){
    echo 'TEST_ENVIRONMENT must be defined before attempting to initialize the DB connection.';
    exit;
}


//assumes $test_path is /Tests
$test_path = realpath(dirname(__FILE__).'/../../');

defined('TEST_PATH')
|| define('TEST_PATH', $test_path);


class TestApplication {
    private static $local_db;
    public static function Database() {
        if (!is_a(self::$local_db, "PDO")) {

            require_once (TEST_PATH.'/../application/config/config.php');

            if(empty(DB_NAME)) {
                echo 'unable to load configs';
                exit;
            }

            //echo "\nTEST DB_NAME = ".DB_NAME."\n"; exit;


            try {
                if(empty(DB_HOST) || !defined('DB_NAME') || empty(DB_USER) || empty(DB_PASS) ){
                    echo 'Unable to load db configs.';
                    exit;
                }

                self::$local_db = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=utf8",DB_USER, DB_PASS);
                self::$local_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::$local_db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

                if(!empty(self::$local_db)){
                    echo "Connected to database on ".TEST_ENVIRONMENT." environment\n";
                }else{
                    echo "Failed to connected to database on ".TEST_ENVIRONMENT." environment\n";
                }

            } catch(Exception $e){
                echo 'Unable to connect to database. Exception thrown: '.$e->getMessage()."\n";
                exit;
            }

        }
        return self::$local_db;
    }
}




function dropDbTables($displayOutputMessages = true){
    $db = TestApplication::Database();
    $dbname = DB_NAME;
    echo "\nDB_NAME = ".DB_NAME."\n";

    $dropTableSql = "SELECT concat('SET foreign_key_checks = 0; DROP TABLE IF EXISTS `', table_name, '`;SET foreign_key_checks = 1;') as query
	FROM information_schema.tables
	WHERE table_schema = :dbName";

    $statement = $db->prepare($dropTableSql);

    $result = $statement->execute(array(':dbName'=>$dbname));

    if($result)
    {
        $result = true;
        $resultset = $statement->fetchAll(\PDO::FETCH_ASSOC);
        foreach($resultset as $aResult)
        {

            try {
                $dropResult = $db->exec($aResult['query']);
            } catch (PDOException $e) {
                $dropResult = false;
                echo "\n".'Database Error, Unable to execute SQL "'.$aResult['query'].'"'."\n".$e->getMessage()."\n\n";
            }

            //if we can't drop it, them it probably has a fk constraint
            if($dropResult === false){
                echo "\n".'Can not drop table, it probably has a foreign key constraint, "'.$aResult['query'].'"'."\n";
                $result = false;
                $displayOutputMessages = false;
            }else{
                if($displayOutputMessages){
                    echo "\n".$aResult['query']." OK dropped!";
                }
            }
        }
    }

    return $result;
}

function createDbTales($displayOutputMessages = true){
    $db = TestApplication::Database();

    //install the db
    $schemaFiles = scandir(TEST_PATH.'/Database/dbv/data/schema');
    if(!empty($schemaFiles) && is_array($schemaFiles)){

        if($displayOutputMessages){
            echo "\n*** BEGIN CREATING DATABASE SCHEMA TABLES ***\n";
        }

        foreach($schemaFiles as $aFileName){

            if(!strstr($aFileName,'.sql')){
                continue;
            }

            if($displayOutputMessages){
                echo "executing $aFileName\n";
            }
            $dataSql = file_get_contents(TEST_PATH."/Database/dbv/data/schema/$aFileName");

            if(!empty($dataSql)){

                try {
                    $result = $db->exec($dataSql);
                } catch (PDOException $e) {
                    $result = false;
                    echo "\n".'Database Error, Unable to execute SQL "'.$dataSql.'"'."\n".$e->getMessage()."\n\n";
                }

                if($result === false){
                    echo "Unable to execute SQL from $aFileName \n "; //db error:\n".print_r($db->errorInfo(), true);
                    //something bad happened.
                    return false;
                }else{
                    if($displayOutputMessages){
                        echo "SQL from $aFileName was executed.\n";
                    }
                }

            }else{
                //empty file or cannot read file?
                echo "No SQL to execute in /Tests/Database/dbv/data/schema/$aFileName\n";
                return false;
            }
        }

        if($displayOutputMessages){
            echo "\n*** END CREATING DATABASE SCHEMA TABLES ***\n";
        }
    }

    return true;
}

function insertTableData($displayOutputMessages = true){
    $db = TestApplication::Database();

    //insert the default db data
    $dataFiles = scandir(TEST_PATH.'/Database/dbv/data/default');
    if(!empty($dataFiles) && is_array($dataFiles)){

        if($displayOutputMessages){
            echo "\n*** BEGIN INSERTING DEFAULT DATA ***\n";
        }

        foreach($dataFiles as $aFileName){

            if(!strstr($aFileName,'.sql')){
                continue;
            }
            if($displayOutputMessages){
                echo "executing $aFileName\n";
            }
            $dataSql = file_get_contents(TEST_PATH."/Database/dbv/data/default/$aFileName");
            if(!empty($dataSql)){
                $dataSql = explode(";\n",$dataSql);
                foreach($dataSql as $aSqlStatement){
                    $aSqlStatement = trim($aSqlStatement);

                    if(empty($aSqlStatement)){
                        continue;
                    }

                    try {
                        $result = $db->exec($aSqlStatement);
                    } catch (PDOException $e) {
                        $result = false;
                        echo "\n".'Database Error, Unable to execute SQL "'.$aSqlStatement.'"'."\n".$e->getMessage()."\n\n";
                    }

                    if($result === false){
                        echo "Unable to insert SQL from $aFileName.\n "; //db error:\n".print_r($db->errorInfo(), true);
                        return false;
                    }
                }

                if($displayOutputMessages){
                    echo "Data from $aFileName was inserted.\n";
                }

            }
            else{

                if($displayOutputMessages){
                    echo "No data to insert. No SQL found in /Tests/Database/dbv/data/default/$aFileName\n";
                }

            }
        }

        if($displayOutputMessages){
            echo "\n*** END INSERTING DEFAULT DATA ***\n";
        }

        return true;
    }

    return false;
}
