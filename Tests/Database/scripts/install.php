<?php
/**
 * example command from /var/www/html/postcard/Tests/Database dir:
 * php scripts/install.php env=unittesting dropTables=true
 *
 */

echo "\n".'-------------------------'."\n";

function printUsage($param){
    echo "\nInvalid Parameter \"$param\".\n\n\nUsage: php scripts/install.php <param>=<value>\n(Assumes you are in Tests/Database dir)..\n\n\nRequired Parameter:\nenv=[local|unittesting|development|staging|production]\n\nOptional Parameter:\ndropTables=true\n";
    exit;
}

function printRequired($param){
    echo "\nRequired Parameter \"$param\".\n\n\nUsage: php scripts/install.php <param>=<value>\n(Assumes you are in Tests/Database dir).\n\n\nRequired Parameter:\nenv=[local|unittesting|development|staging|production]\n\nOptional Parameter:\ndropTables=true\n";
    exit;
}

$validParams = array('env','dropTables');
$validEnv = array('local','unittesting','development','staging','production');

//get arguments given on command line
global $argv;

$params = array();
for($i=1;$i < count($argv);$i++){
    //split the argument name and value
    list($name,$value) = explode("=",$argv[$i]);

    if(!in_array($name,$validParams)){
        printUsage($name);
    }

    if(!empty($name) && !empty($value)){

        if($name == 'env' && !in_array($value,$validEnv)){
            printUsage($name);
        }

        $params[$name] = $value;
    }
}

if(empty($params['env'])){
    printRequired('env');
}

define('TEST_ENVIRONMENT', $params['env']);

require_once('initDb.php');

echo "\n*** TEST_ENVIRONMENT: ".TEST_ENVIRONMENT." ***\n";

if(isset($params['dropTables']) && $params['dropTables'] == true )
{
    $dropTables = false;
    //make sure all tables get dropped.
    while($dropTables == false){
        $dropTables = dropDbTables();
    }

    echo "\nAll Tables dropped from database.\n";
}

//create the tables
$result = createDbTales();

//if we successfully created the tables, then insert the default data
if($result){
    insertTableData();
}
