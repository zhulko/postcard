<div class="container">
    <div class="row">
        <div id="message_sent" class="hidden"></div>
        <div id="message">
            <div class="half">
                <h3 class="clearfix">Your Message</h3>
                <form action="sendmessage" method="post" id="message_form" class="prevent_auto_submit">
                    <input type="hidden" name="p_id" value="<?php echo $p_id; ?>">
                    <input type="hidden" name="message_id" value="<?php echo $this_message_id; ?>">
                    <input type="text" name="subject" value="">

                    <input type="text" name="message_email" value="<?php echo (!empty($this_message->message_email))?json_decode($this_message->message_email):''; ?>" placeholder="Recipient E-Mail">

                    <textarea name="message_content" placeholder="Your Message"><?php echo (!empty($this_message->message_content))?json_decode($this_message->message_content):''; ?></textarea>

                    <hr>
                    <input type="button" name="send_message" id="send_message" value="Send">

                </form>
            </div>
            <div class="half">
                <div class="image_frame">
                    <img class="new_postcard" src="/postcards/showPostcard/<?php echo $p_id; ?>" />
                </div>
            </div>
        </div>
    </div>

</div>