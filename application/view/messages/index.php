<div class="container">
    <h3 class="clearfix">My Sent Messages</h3>
    <table>
        <thead>
        <tr>
            <th>ID</th>
            <th>DATE SENT</th>
            <th>NOTE</th>
            <th>POSTCARD</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($all_messages as $m): ?>
        <tr>
            <td><?php echo $m->message_id; ?></td>
            <td>
                <?php if ($m->message_sent) : ?>
                    <p>Sent on:<br><?php echo date('F j, Y \a\t H:i', strtotime($m->message_sent)); ?></p>
                    <p>Sent to:<br><?php echo json_decode($m->message_email); ?></p>
                <?php else : ?>
                    <p>This message have not been sent yet.<br><a href="/messages/editmessage/<?php echo $m->message_id; ?>">Edit and send</a></p>
                <?php endif; ?>
            </td>
            <td><?php echo json_decode($m->message_content); ?></td>
            <td><img src="<?php echo URL; ?>/postcards/showPostcard/<?php echo $m->p_id; ?>" style="max-width:20vw;width:100%;" /></td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

</div>