<div class="container">
    <h3 class="clearfix">List of postcards (<?php echo $amount_of_postcards; ?>)</h3>
    <div class="cards">
        <?php foreach ($postcards as $postcard) { ?>
            <div class="card">
                <div class="image" style="background-image:url(/images/showThumb/<?php echo $postcard->img_id; ?>);">&nbsp;</div>
                <div class="title"><a href="<?php echo URL . 'postcards/selectpostcard/'. htmlspecialchars($postcard->p_id, ENT_QUOTES, 'UTF-8'); ?>"><?php echo htmlspecialchars(\PCApp\Controller\PostcardsController::getPostcardTitle($postcard->p_text), ENT_QUOTES, 'UTF-8'); ?></a></div>

                <div class="details">
                    <div class="date"><?php echo date('F j, Y', strtotime($postcard->p_modified)); ?></div>
                    <div class="delete"><a href="<?php echo URL . 'postcards/deletepostcard/' . htmlspecialchars($postcard->p_id, ENT_QUOTES, 'UTF-8'); ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>