<form id="postcard_data">
    <input type="hidden" name="img_id" value="<?php echo (empty($_SESSION['new_img_id']))?$postcard->img_id:$_SESSION['new_img_id']; ?>">
</form>
<div class="container">

    <div class="postcard_wrap" id="postcard">
        <div class="postcard_tab tab_left">
            <a href="<?php echo URL; ?>images" class="tab_icon"><i class="fa fa-picture-o" aria-hidden="true"></i> <span>Change Image</span></a>
            <!--a href="<?php echo URL; ?>"><i class="fa fa fa-font" aria-hidden="true"></i> Font</a-->
        </div>
        <div class="postcard_tab tab_right">
            <a href="<?php echo URL; ?>messages/addmessage/<?php echo ( !empty($postcard->p_id ))?$postcard->p_id:''; ?>" class="tab_icon"><i class="fa fa-share" aria-hidden="true"></i> <span>Share</span></a>
        </div>
        <div class="postcard_image" style="background-image: url('/images/showImage/<?php echo (empty($_SESSION['new_img_id']))?$postcard->img_id:$_SESSION['new_img_id']; ?>')">
            <div class="postcard_content">
            <?php foreach ($postcard->texts as $k=>$t) { ?>
                <div class="postcard_text postcard_header resizable draggable" em="<?php echo ($t['postcard-text-size']/16); ?>" px="<?php echo $t['postcard-text-size']; ?>" contenteditable="true" style="
                            font-size:<?php echo ($k==0)?6:4; ?>vw;
                            margin-top:<?php echo $t['postcard-text-coord']['top']; ?>px;
                            margin-left:<?php echo $t['postcard-text-coord']['left']; ?>px;
                            width:<?php echo $t['postcard-text-width']; ?>px;
                            height:<?php echo $t['postcard-text-height']; ?>px;"><?php echo $t['postcard-text']; ?></div>
            <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php unset($_SESSION['new_img_id']); ?>