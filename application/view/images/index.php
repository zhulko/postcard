<div class="container">

    <div class="row">
        <div class="half">

            <div id="drop_area">
                <form class="drop_upload">
                    <label class="center" for="file_to_upload"><i class="button icon fa fa-plus"></i> <span class="icon_label">Add new images</span></label>
                    <input type="file" id="file_to_upload" name="file_to_upload" multiple accept="image/*">
                </form>
                <progress id="progress_bar" max=100 value=0></progress>
                <ul class="info"><li>Drop here</li></ul>
                <ul id="list_uploading_images" ></ul>
            </div>

        </div>
        <div class="half">
            <h3>Select image</h3>
            <div id="uploaded_images">
                <div class="cards">
                    <?php foreach ($images as $image) { ?>
                        <div class="card">
                            <div class="image" style="background-image:url(/images/showThumb/<?php echo $image->img_id; ?>);">&nbsp;</div>
                            <div class="title"><a href="<?php echo URL . 'images/selectimage/'. htmlspecialchars($image->img_id, ENT_QUOTES, 'UTF-8'); ?>"><?php if (isset($image->img_title)) echo htmlspecialchars($image->img_title, ENT_QUOTES, 'UTF-8'); ?></a></div>

                            <div class="details">
                                <div class="check"><a href="<?php echo URL . 'images/selectimage/' . htmlspecialchars($image->img_id, ENT_QUOTES, 'UTF-8'); ?>"><i class="fa fa-check" aria-hidden="true"></i></a></div>
                                <div class="delete"><a href="<?php echo URL . 'images/deleteimage/' . htmlspecialchars($image->img_id, ENT_QUOTES, 'UTF-8'); ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>




</div>
