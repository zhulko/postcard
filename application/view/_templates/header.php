<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Postcard Creator</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- JS -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo URL; ?>js/application.js?t=<?php echo date('r') ?>"></script>

    <!-- CSS -->
    <link href="<?php echo URL; ?>css/style.css?t=<?php echo date('r') ?>" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body class="">
    <div id="ajax_response_panel">
        <i class="ajax_response_panel_close fa fa-times"></i>
        <div id="ajax_response"></div>
    </div>
    <div id="upload_error_panel">
        <i class="upload_error_panel_close fa fa-times"></i>
        <div id="upload_error"></div>
    </div>


    <!-- navigation -->
    <div class="navigation">
        <a href="<?php echo URL; ?>postcards/newpostcard" class="icon"><i class="fa fa-plus" aria-hidden="true"></i></a>
        <a href="<?php echo URL; ?>postcards/newpostcard">Postcard Creator</a>
        <a href="<?php echo URL; ?>postcards">My Postcards</a>
        <a href="<?php echo URL; ?>images">My Images</a>
    </div>
    <div class="container_row">



