<?php

namespace PCApp\Libs;

class Helper
{
    /**
     * debugPDO
     *
     * Shows the emulated SQL query in a PDO statement. What it does is just extremely simple, but powerful:
     * It combines the raw query and the placeholders. For sure not really perfect (as PDO is more complex than just
     * combining raw query and arguments), but it does the job.
     *
     * @author Panique
     * @param string $raw_sql
     * @param array $parameters
     * @return string
     */
    static public function debugPDO($raw_sql, $parameters) {

        $keys = array();
        $values = $parameters;

        foreach ($parameters as $key => $value) {

            // check if named parameters (':param') or anonymous parameters ('?') are used
            if (is_string($key)) {
                $keys[] = '/' . $key . '/';
            } else {
                $keys[] = '/[?]/';
            }

            // bring parameter into human-readable format
            if (is_string($value)) {
                $values[$key] = "'" . $value . "'";
            } elseif (is_array($value)) {
                $values[$key] = implode(',', $value);
            } elseif (is_null($value)) {
                $values[$key] = 'NULL';
            }
        }

        /*
        echo "<br> [DEBUG] Keys:<pre>";
        print_r($keys);

        echo "\n[DEBUG] Values: ";
        print_r($values);
        echo "</pre>";
        */

        $raw_sql = preg_replace($keys, $values, $raw_sql, 1, $count);

        return $raw_sql;
    }


	/**
     * hashFilename
     *
     * make a new file name by hashing user_id _ and hash of the original filename
     *
     * @param string $filename
     * @param bool $with_extension return with file extension or without
     * @param array $_SESSION
     * @return string
     */
    static public function hashFilename($filename, $with_extension = true) {
        $hashed_filename = md5($_SESSION['user_id']).'_'.md5($filename);
        preg_match('/\.([^\.]*$)/', $filename, $extension);
        if (is_array($extension) && sizeof($extension) > 0) {
            $file_extension = strtolower($extension[1]);
        } else {
            $file_extension = '';
        }
        if ($with_extension) {
            return $hashed_filename . '.' . $file_extension;
        } else {
            return $hashed_filename;
        }
    }

}
