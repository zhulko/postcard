<?php

/**
 * Class Postcard
 *
 *
 */

namespace PCApp\Model;


use PCApp\Core\Model;
use PCApp\Libs\Helper;


class Postcard extends Model
{
    /**
     * Get all postcards from database
     */
    public function getAllPostcards()
    {
        $sql = "SELECT * FROM postcards WHERE user_id = :user_id ORDER BY p_modified DESC";
        $query = $this->db->prepare($sql);
        $parameters = array(':user_id' => $_SESSION['user_id']);
        $query->execute($parameters);

        return $query->fetchAll();
    }

    /**
     * Add a postcard to database
     * @param array $postcard_data form values, like img_id, p_text (json array containing postcard texts, text sizes and positions)
     * 
     * return int New postcard id
     */
    public function addPostcard($postcard_data)
    {
        if (!empty($postcard_data['img_id']) && !empty($_SESSION['user_id'])) {
            $img_id = $postcard_data['img_id'];
            unset($postcard_data['img_id']);
            $sql = "INSERT INTO postcards (user_id, img_id, p_text, p_created, p_modified) VALUES (:user_id, :img_id, :p_text, NOW(), NOW())";
            $query = $this->db->prepare($sql);
            $parameters = array(':user_id' => $_SESSION['user_id'], ':img_id' => $img_id, ':p_text' => json_encode($postcard_data));

            // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

            $query->execute($parameters);
            return $this->db->lastInsertId();
        }
    }


    /**
     * Update a postcard in database
     * @param array $postcard_data form values, like img_id, p_text (json array containing postcard texts, text sizes and positions)
     */
    public function updatePostcard( $postcard_data )
    {
        if (!empty($postcard_data['img_id']) && !empty($_SESSION['user_id']) && !empty($_SESSION['p_id'])) {
            $img_id = $postcard_data['img_id'];
            unset($postcard_data['img_id']);
            $sql = "UPDATE postcards SET p_text = :p_text, img_id = :img_id, p_modified = NOW() WHERE p_id = :p_id AND user_id = :user_id";
            $query = $this->db->prepare($sql);
            $parameters = array(':p_id' => $_SESSION['p_id'], ':p_text' => json_encode($postcard_data), ':img_id' => $img_id, ':user_id' => $_SESSION['user_id']);

            // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

            $query->execute($parameters);
        }
    }

    /**
     * Delete a postcard in the database
   	 * @param int $p_id Id of postcard
   	 * and user id from $_SESSION
     */
    public function deletePostcard($p_id)
    {
        $sql = "DELETE FROM postcards WHERE p_id = :p_id AND user_id = :user_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array(':p_id' => $p_id, ':user_id' => $_SESSION['user_id']);

        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);
    }


    /**
     * Get a postcard by image id from database
     * @param integer $img_id
     *
     * return int Postcard id or false
     */
    public function getPostcardIdByImgId($img_id)
    {

        $sql = "SELECT p_id FROM postcards WHERE img_id = :img_id AND user_id = :user_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array(':img_id' => $img_id, ':user_id' => $_SESSION['user_id']);

        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);

        return ($query->rowcount() ? $query->fetch()->p_id : false);
    }


    /**
     * Get a postcard fields from database
     * @param integer $p_id
     *
     * return object postcard or false
     */
    public function fetchPostcard($p_id)
    {
        $sql = "SELECT * FROM postcards WHERE p_id = :p_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array(':p_id' => $p_id);

        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);

        // fetch() is the PDO method that get exactly one result
        return ($query->rowcount() ? $query->fetch() : false);
    }


	/**
     * Get an id of the last edited by session user postcard from database
     *
     * return int Id of postcard or false
     */
    public function fetchNewestPostcardId()
    {
        $sql = "SELECT p_id FROM postcards WHERE user_id = :user_id ORDER BY p_modified DESC LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array(':user_id' => $_SESSION['user_id']);

        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);

        return ($query->rowcount() ? $query->fetch()->p_id : false);
    }



    /**
     * Get number of user postcards
     *
     * return int Number of postcards
     */
    public function getAmountOfPostcards()
    {
        $sql = "SELECT COUNT(p_id) AS amount_of_postcards FROM postcards WHERE user_id = :user_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':user_id' => $_SESSION['user_id']);
        $query->execute($parameters);

        return $query->fetch()->amount_of_postcards;
    }
}
