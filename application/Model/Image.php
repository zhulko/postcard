<?php

/**
 * Class Image
 *
 */

namespace PCApp\Model;

use PCApp\Core\Model;
use PCApp\Libs\Helper;

class Image extends Model
{
    /**
     * Get all images from database
     */
    public function getAllImages()
    {
        $sql = "SELECT img_id, img_title, img_filename, img_created, img_modified, img_source  FROM images WHERE user_id = :user_id ORDER BY img_created DESC";
        $query = $this->db->prepare($sql);
        $parameters = array(':user_id' => $_SESSION['user_id']);
        $query->execute($parameters);
        return $query->fetchAll();
    }

    /**
     * Add a image to database
     * @param string $img_filename Image File Name
     * @param string $img_exif Image EXIF
     * @param array $post Other details from the upload form, maybe how image was added upload/capture, or maybe image size, type
     * @param array $_SESSION for User Id
     *
     * @return Int inserted id
     */
    public function addImage($img_filename, $img_exif=null, $post=null)
    {
        $sql = "INSERT INTO images (user_id, img_filename, img_exif, img_source, img_created, img_modified) VALUES (:user_id, :img_filename, :img_exif, :img_source, NOW(), NOW())";
        $query = $this->db->prepare($sql);
        $parameters = array(':user_id' => $_SESSION['user_id'], ':img_filename' => $img_filename, ':img_exif' => json_encode($img_exif), ':img_source' => $post['img_source'] );

        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);
        return $this->db->lastInsertId();
    }

    /**
     * Delete a image from the database
     * Please note: this is just an example! In a real application you would not simply let everybody
     * add/update/delete stuff!
     * @param int $img_id Id of image
     */
    public function deleteImage($img_id)
    {
        $sql = "DELETE FROM images WHERE img_id = :img_id AND user_id = :user_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':img_id' => $img_id, ':user_id' => $_SESSION['user_id']);

        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);
    }


    /**
     * Get a Image Id by Image File name from database
     * @param integer $img_filename
     *
     * @return integer Image Id
     */
    public function getImageIdByFilename($img_filename)
    {

        $sql = "SELECT img_id FROM images WHERE img_filename = :img_filename AND user_id = :user_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array(':img_filename' => $img_filename, ':user_id' => $_SESSION['user_id']);

        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);

        return ($query->rowcount() ? $query->fetch()->img_id : false);
    }


    /**
     * Get a image from database
     * @param integer $img_id
     *
     * return object for image details
     */
    public function getImage($img_id)
    {
        $sql = "SELECT * FROM images WHERE img_id = :img_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array(':img_id' => $img_id);

        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);

        return ($query->rowcount() ? $query->fetch() : false);
    }

    /**
     * Update a image in database
     * @param int $img_id Image id
     * @param string $img_exif EXIF
     * @param string $post Other fields from the update form
     */
    public function updateImage( $img_id, $img_exif, $post)
    {
        $sql = "UPDATE images SET img_title = :img_title, img_exif = :img_exif, img_source = :img_source WHERE img_id = :img_id AND user_id = :user_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':img_title' => $post['img_title'], ':img_exif' => $img_exif, ':img_source' => $post['img_source'], ':img_id' => $img_id, ':user_id' => $_SESSION['user_id']);

        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);
    }

    /**
     * Get number of images for this user
     *
     * return int Number of images
     */
    public function getAmountOfImages()
    {
        $sql = "SELECT COUNT(img_id) AS amount_of_images FROM images WHERE user_id = :user_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':user_id' => $_SESSION['user_id']);
        $query->execute($parameters);

        // fetch() is the PDO method that get exactly one result
        return $query->fetch()->amount_of_images;
    }
}
