<?php

/**
 * Class Message
 *
 *
 */

namespace PCApp\Model;


use PCApp\Core\Model;
use PCApp\Libs\Helper;


class Message extends Model
{
    /**
     * Get all messages from database
     */
    public function getAllMessages()
    {
        $sql = "SELECT * FROM messages WHERE user_id = :user_id ORDER BY message_sent DESC";
        $query = $this->db->prepare($sql);
        $parameters = array(':user_id' => $_SESSION['user_id']);
        $query->execute($parameters);

        return $query->fetchAll();
    }

    /**
     * Add a new blank message to database
     * @param int $p_id Postcard Id
     *
     * return int New message id
     */
    public function addMessage($p_id)
    {
        if (!empty($p_id) && !empty($_SESSION['user_id'])) {

                $sql = "INSERT INTO messages (user_id, p_id, message_created) VALUES (:user_id, :p_id, NOW())";
                $query = $this->db->prepare($sql);
                $parameters = array(':user_id' => $_SESSION['user_id'], ':p_id' => $p_id);

                // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

                $query->execute($parameters);
                return $this->db->lastInsertId();
        }
    }

    /**
     * Check if message was sent, message_sent value is not null 
     * @param int $p_id Postcard Id
     *
     * return int Matching message id
     */
    public function checkIfMessageUnsent($p_id)
    {
        if (!empty($p_id) && !empty($_SESSION['user_id'])) {

            $sql = "SELECT message_id FROM messages WHERE message_sent IS NULL AND p_id = :p_id AND user_id = :user_id LIMIT 1";
            $query = $this->db->prepare($sql);
            $parameters = array(':p_id' => $p_id, ':user_id' => $_SESSION['user_id']);

            // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

            $query->execute($parameters);
            return ($query->rowcount() ? $query->fetch()->message_id : false);
        }
    }



    /**
     * Delete message from the database
     * @param int $message_id Id of message
     */
    public function deleteMessage($message_id)
    {
        $sql = "DELETE FROM messages WHERE message_id = :message_id AND user_id = :user_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array(':message_id' => $message_id, ':user_id' => $_SESSION['user_id']);

        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);
    }


    /**
     * Get a message id from postcard id from database
     * @param integer $p_id Postcard Id
     *
     * return int Matching message id
     */
    public function getMessageIdByPostcardId($p_id)
    {

        $sql = "SELECT message_id FROM messages WHERE p_id = :p_id AND user_id = :user_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array(':p_id' => $p_id, ':user_id' => $_SESSION['user_id']);

        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);

        return ($query->rowcount() ? $query->fetch()->message_id : false);
    }


    /**
     * Get a message from database
     * @param int $message_id
     *
     * return int Message id
     */
    public function getMessage($message_id)
    {
        $sql = "SELECT * FROM messages WHERE message_id = :message_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array(':message_id' => $message_id);

        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);

        return ($query->rowcount() ? $query->fetch() : false);
    }

    /**
     * Update a message in database, add message details like email and message text
     * @param array $post
     *
     */
    public function updateMessage( $post )
    {
        if (!empty($post['message_id']) && !empty($_SESSION['user_id']) && !empty($post['p_id'])) {
            $sql = "UPDATE messages SET 
              message_email = :message_email,
              message_content = :message_content,
              message_created = NOW() 
            WHERE
              message_id = :message_id AND
              user_id = :user_id";
            $query = $this->db->prepare($sql);
            $parameters = array(
                ':message_id' => filter_var($post['message_id'],FILTER_SANITIZE_NUMBER_INT),
                ':message_email' => json_encode(filter_var($post['message_email'],FILTER_SANITIZE_EMAIL)),
                ':message_content' => json_encode(filter_var($post['message_content'],FILTER_SANITIZE_STRING)),
                ':user_id' => $_SESSION['user_id']
            );

            // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

            $query->execute($parameters);
        }
    }






	/**
     * Mark message as sent
     * @param int $message_id Message Id
     *
     */
    public function setMessageAsSent( $message_id )
    {
        if (!empty($message_id) && !empty($_SESSION['user_id'])) {
            $sql = "UPDATE messages SET message_sent = NOW() WHERE message_id = :message_id AND user_id = :user_id";
            $query = $this->db->prepare($sql);
            $parameters = array(
                ':message_id' => filter_var($message_id,FILTER_SANITIZE_NUMBER_INT),
                ':user_id' => $_SESSION['user_id']
            );

            // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

            $query->execute($parameters);
        }
    }



    /**
     * Get number of user messages
     */
    public function getAmountOfMessages()
    {
        $sql = "SELECT COUNT(message_id) AS amount_of_messages FROM messages WHERE user_id = :user_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':user_id' => $_SESSION['user_id']);
        $query->execute($parameters);

        return $query->fetch()->amount_of_messages;
    }
}
