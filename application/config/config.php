<?php

/**
 * Configuration
 *
 */

//ini_set('memory_limit', '256M');

/**
 * Configuration for: Error reporting
 */
define('ENVIRONMENT', 'development');

if (ENVIRONMENT == 'development' || ENVIRONMENT == 'dev') {
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
}

/**
 *
 */

define('URL_PUBLIC_FOLDER', 'public');
define('URL_PROTOCOL', '//');
if (!defined('TEST_ENVIRONMENT')) {
    define('URL_DOMAIN', $_SERVER['HTTP_HOST']);
    define('URL_SUB_FOLDER', str_replace(URL_PUBLIC_FOLDER, '', dirname($_SERVER['SCRIPT_NAME'])));
    define('URL', URL_PROTOCOL . URL_DOMAIN . URL_SUB_FOLDER);
}
/**
 * Configuration for: Database
 * This is the place where you define your database credentials, database type (mysql, pgsql)
 */

if (defined('TEST_ENVIRONMENT') && TEST_ENVIRONMENT == 'unittesting') {
    define('DB_TYPE', 'mysql');
    define('DB_HOST', '127.0.0.1');
    define('DB_NAME', 'pc_app_unittesting');
    define('DB_USER', 'root');
    define('DB_PASS', '12345678');
} else {
    define('DB_TYPE', 'mysql');
    define('DB_HOST', '127.0.0.1');
    define('DB_NAME', 'pc_app');
    define('DB_USER', 'root');
    define('DB_PASS', '12345678');
}
define('DB_CHARSET', 'utf8mb4');
# define('DB_FILE', '../data/sqlite.db');


/**
 * Defined paths for uploads and images
 */
define('UPLOADS_PATH', '../uploads/');
define('IMAGE_ORIGINAL_PATH', 'originals/');
define('IMAGE_PROCESSED_PATH', 'images/');
define('IMAGE_THUMB_PATH', 'thumbs/');
define('POSTCARD_PATH', 'postcards/');
define('FONTS_PATH', '../public/fonts/');

/**
 * upload file settings
 */
define('FILE_FORBIDDEN', 'application/*');
define('FILE_ALLOWED', 'image/*');
define('MAXFILE_SIZE', '25000000');


/**
 * Default values for Postcard texts and testing
 */
define('DEFAULT_TEXT', '{"postcard-text-coord_0":"20,20","postcard-text_0":"New Postcard Header","postcard-text-size_0":"70","postcard-text-coord_1":"110,20","postcard-text_1":"New Postcard body, click to edit","postcard-text-size_1":"50"}');
define('DEFAULT_IMG_ID', '36');
define('DEFAULT_USER_ID', '6');
define('DEFAULT_USER_EMAIL', 'vzhulkovsky@gmail.com');

define('MESSAGE_SEND_SUCCESS', 'Thank you, your message has been sent.');
define('MESSAGE_SEND_ERROR', 'Error');


$_SESSION['user_id'] = DEFAULT_USER_ID;

