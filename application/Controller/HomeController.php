<?php

/**
 * Class HomeController
 *
 *
 */

namespace PCApp\Controller;
use PCApp\Model\Postcard;

class HomeController
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index()
    {
    	/**
    	 * Get edited by user postcard
    	 */
        $p_id = null;
        if (!empty($_SESSION['p_id'])) {
            $p_id = filter_var($_SESSION['p_id'], FILTER_SANITIZE_NUMBER_INT);
        }
        $PostcardsController = new PostcardsController();
        $postcard = $PostcardsController->getPostcard($p_id);

        /*
        if (empty($_SESSION['img_id'])) {
            $_SESSION['img_id'] = $postcard->img_id;
        }
        */
        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/home/index.php';
        require APP . 'view/_templates/footer.php';
    }


    /**
     * AJAX-ACTION: savePostcardData
     * TODO documentation
     */
    public function savePostcardData()
    {
        $postcard_data = $_POST;
        $Postcard = new Postcard();
        if (!empty($_SESSION['p_id'])) {
            $Postcard->updatePostcard($postcard_data);
        } else {
            $_SESSION['p_id'] = $Postcard->addPostcard($postcard_data);
        }

    }



}
