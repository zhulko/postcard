<?php

namespace PCApp\Controller;

use PCApp\Model\Postcard;


class PostcardsController
{
    public function index()
    {

        $Postcard = new Postcard();
        $postcards = $Postcard->getAllPostcards();
        $amount_of_postcards = $Postcard->getAmountOfPostcards();

        require APP . 'view/_templates/header.php';
        require APP . 'view/postcards/index.php';
        require APP . 'view/_templates/footer.php';
    }



    public function getPostcard($p_id = null) {
        $Postcard = new Postcard();
        $this_postcard = $Postcard->fetchPostcard($p_id);
        if ($this_postcard == false) {
            $this_postcard = (object)array();
        }
        if (empty($this_postcard->p_text)) {
            $this_postcard->p_text = DEFAULT_TEXT;
            $this_postcard->img_id = DEFAULT_IMG_ID;
        }

        $texts = array();
        foreach (json_decode($this_postcard->p_text) as $key => $val) {
            list($field, $order) = explode('_', $key);
            if ($field == 'postcard-text-coord') {
                list($top, $left) = explode(',', $val);
                $margin['top'] = $top;
                $margin['left'] = $left;
                $texts[$order][$field] = $margin;
            } else {
                $texts[$order][$field] = urldecode($val);
            }
        }
        $this_postcard->texts = $texts;
        return $this_postcard;
    }



    public static function getPostcardTitle($p_text) {
        preg_match("/\"postcard-text_0\":\"(.*?)\"/",$p_text,$m);
        return urldecode($m[1]);
    }



    public function selectPostcard($p_id)
    {
        if (!empty($p_id)) {
            $p_id = filter_var($p_id, FILTER_SANITIZE_NUMBER_INT);
            $this_postcard = $this->getPostcard($p_id);

            if ($this_postcard) {
                $_SESSION['p_id'] = $this_postcard->p_id;
                header('location: ' . URL );
            }
        }
    }


    public function deletePostcard($p_id)
    {
        // if we have an id of the Postcard that should be deleted
        if (isset($p_id)) {
            // Instance new Model (Postcard)
            $Postcard = new Postcard();
            $Postcard->deletePostcard($p_id);
        }

        // where to go after Postcard has been deleted
        header('location: ' . URL . 'postcards/index');
    }




    public function getPostcardText($p_id)
    {

    }


    public function showPostcard($p_id)
    {
        $postcard = $this->getPostcard($p_id);
        $ImagesController = new ImagesController();
        $ImagesController->showImage($postcard->img_id,'postcard', $p_id);
    }



    public function newPostcard()
    {
        unset($_SESSION['p_id']);
        unset($_SESSION['img_id']);
        header('location: ' . URL );
    }

}