<?php

namespace PCApp\Controller;

use PCApp\Model\Image;
use PCApp\Model\Postcard;
use PCApp\Model\Message;
use PCApp\Libs\Upload;
use PCApp\Libs\Encoding;
use PHPMailer\PHPMailer\PHPMailer;


class MessagesController
{
    public function index()
    {
        $Message = new Message();
        $all_messages = $Message->getAllMessages();
        $amount_of_messages = $Message->getAmountOfMessages();

        require APP . 'view/_templates/header.php';
        require APP . 'view/messages/index.php';
        require APP . 'view/_templates/footer.php';
    }



    public function getNewestPostcardId() {
        $Postcard = new Postcard();
        return $Postcard->fetchNewestPostcardId();
    }



    public function addMessage($p_id = null) {
        // if no p_id, try to get last edited postcard
        if (empty($p_id)) {
            $p_id = $this->getNewestPostcardId();
        }
        $this->addTextToImage($p_id);
    }



    public function editMessage($message_id = null) {
        if (!empty($message_id)) {
            $Message = new Message();
            $this_message = $Message->getMessage($message_id);
            $p_id = $this_message->p_id;
        }
        require APP . 'view/_templates/header.php';
        require APP . 'view/messages/message.php';
        require APP . 'view/_templates/footer.php';
    }



    public function addTextToImage($p_id)
    {
        $Message = new Message();

        // check if message already created
        $this_message_id = $Message->checkIfMessageUnsent($p_id);

        if (empty($this_message_id)) {

            $font = FONTS_PATH . 'Times New Roman.ttf';
            $PostcardsController = new PostcardsController();
            $postcard = $PostcardsController->getPostcard($p_id);
            $Image = new Image();
            $selected_image = $Image->getImage($postcard->img_id);
            $Encoding = new Encoding();

            // create blank postcard
            $handle_orig = new Upload(UPLOADS_PATH . IMAGE_ORIGINAL_PATH . $selected_image->img_filename);
            $handle_orig->file_overwrite = true;
            // prepend p_id to the postcard image filename
            $handle_orig->file_name_body_pre = $p_id . '-';
            $handle_orig->process(UPLOADS_PATH . POSTCARD_PATH);
            unset($handle_orig);

            // add texts
            foreach ($postcard->texts as $k => $text) {

                $handle = new Upload(UPLOADS_PATH . POSTCARD_PATH . $p_id . '-' . $selected_image->img_filename);
                $handle->dir_auto_chmod = true;
                if ($handle->uploaded) {

                    // compare scale of original image to website image
                    $postcard_w = $text['postcard-image-width'];
                    $postcard_h = $text['postcard-image-height'];
                    $scale_w = ($handle->image_src_x / 100) / ($postcard_w / 100);
                    $scale_h = ($handle->image_src_y / 100) / ($postcard_h / 100);
                    $scale = ($scale_w < $scale_h) ? $scale_w : $scale_h;

                    // set text size and position on the image
                    $margin_left = ceil($text['postcard-text-coord']['left'] * $scale);
                    $margin_top = ceil($text['postcard-text-coord']['top'] * $scale);
                    $font_size = ceil($text['postcard-text-size'] * $scale);
                    $text_width = ceil($text['postcard-text-width'] * $scale);

                    // get width of the text in this font
                    $ttfbbox = imagettfbbox($font_size, 0, $font, $text['postcard-text']);

                    // get width of one character for this font
                    $character_width = ceil($ttfbbox[3] / strlen($text['postcard-text']));

                    // wrap text
                    $wrap_at_characters = ceil($text_width / $character_width);
                    $image_text = $Encoding->fixUTF8(wordwrap($text['postcard-text'], $wrap_at_characters, "\n"));

                    // add text to image
                    $handle->file_overwrite = true;
                    $handle->image_text = $image_text;
                    $handle->image_text_direction = 'h';
                    $handle->image_text_color = '#ffffff';
                    $handle->image_text_opacity = 100;
                    $handle->image_text_background = '#555555';
                    $handle->image_text_background_opacity = 0;
                    $handle->image_text_font = $font; // or './font.gdf' or './font.ttf'
                    $handle->image_text_size = $font_size;
                    $handle->image_text_position = 'TL';
                    $handle->image_text_padding_x = $margin_left;
                    $handle->image_text_padding_y = $margin_top;
                    $handle->Process(UPLOADS_PATH . POSTCARD_PATH);

                }

                unset($handle);
            }

            $this_message_id = $Message->addMessage($p_id);

        }
        if (!empty($this_message_id)) {
            $this_message = $Message->getMessage($this_message_id);
        }

        require APP . 'view/_templates/header.php';
        require APP . 'view/messages/message.php';
        require APP . 'view/_templates/footer.php';
    }



    public function sendMessage() {

        if (!empty($_POST['message_id']) && ($_POST['message_id'] > 0)) {
            $message_id = filter_var($_POST['message_id'], FILTER_SANITIZE_NUMBER_INT);
            $Message = new Message();
            $Message->updateMessage($_POST);
            $m = $Message->getMessage($message_id);

            if (!empty($m->message_id)) {
                //$PostcardController = new PostcardsController();
                //$p = $PostcardController->getPostcard($m->p_id);

                $m->message_content = filter_var(json_decode($m->message_content),FILTER_SANITIZE_STRING);
                $m->message_content .= '<br><br><img src="'.URL.'/postcards/showPostcard/'.$m->p_id.'" style="width:100%;" />';
                wordwrap($m->message_content, 70);

                require APP . 'Libs/PHPMailer.php';
                require APP . 'Libs/Exception.php';
                $Mail = new PHPMailer();

                $Mail->setFrom('vzhulkovsky@gmail.com', 'Postcard App');
                $Mail->addAddress(filter_var(json_decode($m->message_email),FILTER_SANITIZE_EMAIL),$m->message_email);
                $Mail->addReplyTo('vzhulkovsky@gmail.com', 'Vitali');

                $Mail->isHTML(true);
                $Mail->Subject = 'Postcard for you';
                $Mail->Body = $m->message_content;
                $Mail->AltBody = strip_tags($m->message_content);


                if ($Mail->send()) {
                    echo MESSAGE_SEND_SUCCESS;
                    $Message->setMessageAsSent( $m->message_id );
                    echo '<br><br><h3>Your Message</h3><div class="image_frame">';
                    echo $m->message_content;
                    echo '</div>';
                } else {
                    echo '<div class="error_message"><i class="fa fa-times dismiss"></i><h3>Error:</h3>';
                    echo 'Message could not be sent. Error: ', $Mail->ErrorInfo;
                    echo '</div>';
                }
            }
        }
    }



}