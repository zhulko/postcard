<?php

/**
 * Class ImagesController
 *
 */

namespace PCApp\Controller;


    use PCApp\Libs\Helper;
    use PCApp\Model\Image;
    use PCApp\Libs\Upload;
    use PCApp\Libs\PNGReader;


    class ImagesController
    {
        /**
         * PAGE: index
         * This method handles what happens when you move to /images/index
         */
        public function index()
        {
            // Instance new Model (Image)
            $Image = new Image();
            // getting all images and amount of images
            $images = $Image->getAllImages();
            $amount_of_images = $Image->getAmountOfImages();

            // load views. within the views we can echo out $images and $amount_of_images easily
            require APP . 'view/_templates/header.php';
            require APP . 'view/images/index.php';
            require APP . 'view/_templates/footer.php';
        }


        /**
         * AJAX-ACTION: getUploadedImages
         * Refresh list of shown images after uploading new images on /images
         */
        public function getUploadedImages()
        {
            $Image = new Image();
            $images = $Image->getAllImages();
            echo '<div class="cards">';
            foreach ($images as $image) {
                echo '<div class="card">';
                echo '<div class="image" style="background-image:url(/images/showThumb/';
                echo $image->img_id;
                echo ');">&nbsp;</div>';
                echo '<div class="title"><a href="';
                echo URL . 'images/selectimage/'. htmlspecialchars($image->img_id, ENT_QUOTES, 'UTF-8');
                echo '">';
                if (isset($image->img_title)) echo htmlspecialchars($image->img_title, ENT_QUOTES, 'UTF-8');
                echo '</a></div>';
                echo '<div class="details">';
                echo '<div class="check"><a href="';
                echo URL . 'images/selectimage/' . htmlspecialchars($image->img_id, ENT_QUOTES, 'UTF-8');
                echo '"><i class="fa fa-check" aria-hidden="true"></i></a></div>';
                echo '<div class="delete"><a href="';
                echo URL . 'images/deleteimage/' . htmlspecialchars($image->img_id, ENT_QUOTES, 'UTF-8');
                echo '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
                echo '</div>';
                echo '</div>';
            }
            echo '</div>';
        }

        /**
         * AJAX-ACTION: addImage
         * Process image uploaded with XMLHttpRequest on /images
         */
        public function addImage()
        {
            $upload_success = false;
            //print(file_get_contents('php://input'));
            //var_dump($_SERVER);
            if (isset($_SERVER['HTTP_X_FILE_NAME'])) {
                $upload_string = 'php:' . $_SERVER['HTTP_X_FILE_NAME'];
                $upload_filename = $_SERVER['HTTP_X_FILE_NAME'];
            }
            if (!isset($_SERVER['HTTP_X_FILE_NAME']) && isset($_FILES['file_original'])) {
                $upload_string = $_FILES['file_original'];
                $upload_filename = $_FILES['file_original']['name'];
            }

            if (isset($upload_filename)) {
            
                $hashed_filename = Helper::hashFilename($upload_filename);
                
                // delete image files if same file was uploaded before
                $this->deleteImageFiles($hashed_filename);

                $handle_original = new Upload($upload_string);
                
                // check if image was uploaded successfully
                if ($handle_original->uploaded) {
                
                    // make a hashed filename
                    $hashed_filename_body = Helper::hashFilename($handle_original->file_src_name, false);
                    
                    // define settings for upload class to upload original iage
                    $handle_original->file_safe_name = false;
                    $handle_original->image_ratio_no_zoom_in = true;
                    $handle_original->file_new_name_body = $hashed_filename_body;
                    $handle_original->image_ratio = true;
                    $handle_original->forbidden = array( FILE_FORBIDDEN );
                    $handle_original->allowed   = array( FILE_ALLOWED );
                    $handle_original->jpeg_size = MAXFILE_SIZE;
                    $handle_original->file_overwrite = true;
                    $handle_original->dir_chmod = 0775;
                    $handle_original->process( UPLOADS_PATH . IMAGE_ORIGINAL_PATH);
                    
                    // if original image file was saved successfully
                    if ( $handle_original->processed ) {
                        
                        // try to get exif data
                        if ($handle_original->file_dst_name_ext == 'png') {
                            $png = new PNGReader(UPLOADS_PATH . IMAGE_ORIGINAL_PATH . $handle_original->file_dst_name);
                            $img_exif = $png->get_chunks('eXIf');
                        } else if (exif_read_data(UPLOADS_PATH . IMAGE_ORIGINAL_PATH . $handle_original->file_dst_name)) {
                            $img_exif = @exif_read_data(UPLOADS_PATH . IMAGE_ORIGINAL_PATH . $handle_original->file_dst_name);
                        }
                        
                        // save resized image copy
                        $handle = $handle_original;
                        $handle->file_new_name_body = $hashed_filename_body;
                        $handle->image_resize = true;
                        $handle->image_x = 1000;
                        $handle->image_ratio_y = true;
                        $handle->file_overwrite = true;
                        $handle->dir_chmod = 0775;
                        $handle->process(UPLOADS_PATH . IMAGE_PROCESSED_PATH);
                        if ($handle->processed) {
                            //echo $handle->log;
                            $filename_to_save = $handle->file_dst_name;
                            $upload_success = true;
                        } else {
                            echo 'error: ' .$handle->error;
                            //echo $handle->log;
                        }
                        
                        // save thumbnail size
                        $handle_thumb = $handle_original;
                        $handle_thumb->file_new_name_body = $hashed_filename_body;
                        $handle_thumb->image_resize = true;
                        $handle_thumb->image_x = 200;
                        $handle_thumb->image_ratio_y = true;
                        $handle_thumb->file_overwrite = true;
                        $handle_thumb->dir_chmod = 0775;
                        $handle_thumb->process(UPLOADS_PATH . IMAGE_THUMB_PATH);
                        if ($handle_thumb->processed) {
                            //echo $handle_thumb->log;
                            $handle_thumb->clean();
                        } else {
                            echo 'error: ' . $handle_thumb->error;
                            //echo $handle_thumb->log;
                        }
                        
                        $handle_original->clean();
                        $handle->clean();
                    } else {
                        echo 'error: ' . $handle_original->error;
                        //echo $handle_original->log;
                    }
                } else {
                //    echo $handle_original->log;
                    echo 'error: ' .$handle_original->error;
                }

                if ($upload_success == true) {
                    // Instance new Model (Image)
                    $Image = new Image();

                    // check if image record exists in DB
                    $img_id = $Image->getImageIdByFilename($filename_to_save);

                    if (!$img_id) {
                        // do addImage() in model/model.php
                        $img_id = $Image->addImage($filename_to_save, $img_exif, $_POST);
                    } else {
                        // do updateImage() from model/model.php
                        $Image->updateImage($img_id, $img_exif, $_POST);
                    }

                    echo $img_id;
                }


            }


            // where to go after image has been added
            //header('location: ' . URL . 'images/index');
        }

        /**
         * ACTION: deleteImage Delete database record for this image and image files
         *
         * @param int $img_id Id of the to-delete image
         */
        public function deleteImage($img_id)
        {
            // if we have an id of a image that should be deleted
            if (isset($img_id)) {
                // Instance new Model (Image)
                $Image = new Image();
                $selected_image = $Image->getImage($img_id);
                // delete files
                $this->deleteImageFiles($selected_image->img_filename);
                // delete record do deleteImage() in model/model.php
                $Image->deleteImage($img_id);
            }

            // where to go after image has been deleted
            header('location: ' . URL . 'images/index');
        }



        /**
         * ACTION: deleteImageFiles Deletes all versions of the image file
         *
         * @param string $hashed_filename Image filename
         */
        private function deleteImageFiles($hashed_filename)
        {
            if (isset($hashed_filename)) {
                if (file_exists(UPLOADS_PATH . IMAGE_ORIGINAL_PATH . $hashed_filename )) {
                    @unlink(UPLOADS_PATH . IMAGE_ORIGINAL_PATH . $hashed_filename);
                }
                if (file_exists(UPLOADS_PATH . IMAGE_PROCESSED_PATH . $hashed_filename)) {
                    @unlink(UPLOADS_PATH . IMAGE_PROCESSED_PATH . $hashed_filename);
                }
                if (file_exists(UPLOADS_PATH . IMAGE_THUMB_PATH . $hashed_filename)) {
                    @unlink(UPLOADS_PATH . IMAGE_THUMB_PATH . $hashed_filename);
                }
            }
        }


        /**
         * ACTION: editImage
         * 
         * @param int $img_id Id of the image to edit
         */
        public function editImage($img_id)
        {
            // if we have an id of an image that should be edited
            if (isset($img_id)) {
                // Instance new Model (Image)
                $Image = new Image();
                // do getImage() in model/model.php
                $image = $Image->getImage($img_id);

                // If the image wasn't found, then it would have returned false, and we need to display the error page
                if ($image === false) {
                    $page = new \PCApp\Controller\ErrorController();
                    $page->index();
                } else {
                    // load views. within the views we can echo out $image easily
                    require APP . 'view/_templates/header.php';
                    require APP . 'view/images/edit.php';
                    require APP . 'view/_templates/footer.php';
                }
            } else {
                // redirect user to images index page (as we don't have a image_id)
                header('location: ' . URL . 'images/index');
            }
        }


        /**
         * ACTION: updateImage
         * This method handles what happens when you move to http://yourproject/images/updateimage
         * IMPORTANT: This is not a normal page, it's an ACTION. This is where the "update a image" form on images/edit
         * directs the user after the form submit. This method handles all the POST data from the form and then redirects
         * the user back to images/index via the last line: header(...)
         */
        public function updateImage()
        {
            // if we have POST data to create a new image entry
            if (isset($_POST["submit_update_image"])) {
                // Instance new Model (Image)
                $Image = new Image();
                // do updateImage() from model/model.php
                $Image->updateImage($_POST["img_id"], $_POST["img_exif"], $_POST);
            }

            // where to go after image has been added
            header('location: ' . URL . 'images/index');
        }


        /**
         * ACTION: showThumb display resized image file
         *
         * @param int $img_id Id of the image
         */
        public function showImage($img_id, $img_type = null, $p_id = null)
        {
            if (!empty($img_id)) {
                $file_folder = IMAGE_PROCESSED_PATH;
                $img_id = filter_var($img_id, FILTER_SANITIZE_NUMBER_INT);
                $Image = new Image();
                $img = $Image->getImage($img_id);

                if ($img) {
                    if ($img_type == 'thumb') {
                        $file_folder = IMAGE_THUMB_PATH;
                    } elseif ($img_type == 'postcard') {
                        $file_folder = POSTCARD_PATH . $p_id . '-';
                    }
                    $img_info = getimagesize(UPLOADS_PATH . $file_folder . $img->img_filename);
                    header("Content-type: {$img_info['mime']}");
                    readfile(UPLOADS_PATH . $file_folder . $img->img_filename);
                }
            }
        }


        /**
         * ACTION: showThumb display thumbnail image file
         *
         * @param int $img_id Id of the image
         */
        public function showThumb($img_id)
        {
            $this->showImage($img_id,'thumb');
        }


        /**
         * ACTION: selectImage Select image to use for a postcard, stores image id in $_SESSION['new_img_id'], to use on the homepage
         *
         * @param int $img_id Id of the image
         */
        public function selectImage($img_id)
        {
            if (!empty($img_id)) {
                $img_id = filter_var($img_id, FILTER_SANITIZE_NUMBER_INT);
                $Image = new Image();
                $img = $Image->getImage($img_id);
                if ($img) {
                    $_SESSION['new_img_id'] = $img->img_id;
                    header('location: ' . URL );
                }
            }
        }


    }

