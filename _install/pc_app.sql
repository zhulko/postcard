SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pc_app`
--

CREATE DATABASE IF NOT EXISTS `pc_app_unittesting`;
CREATE DATABASE IF NOT EXISTS `pc_app`;
USE `pc_app`;

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `img_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `img_filename` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `file_modified` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_created` datetime NOT NULL,
  `img_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `img_exif` text COLLATE utf8_unicode_ci,
  `img_source` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_title` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`img_id`),
  UNIQUE KEY `img_id` (`img_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=46 ;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`img_id`, `user_id`, `img_filename`, `file_modified`, `img_created`, `img_modified`, `img_exif`, `img_source`, `img_title`) VALUES
(36, 6, '1679091c5a880faf6fb5e6087eb1b2dc_610584c8a9cc5a53bfc429392949eee6.jpg', NULL, '2018-10-11 14:26:58', '2018-10-15 19:11:59', 'Array', NULL, NULL),
(39, 6, '1679091c5a880faf6fb5e6087eb1b2dc_c96a6245c1bb60890b54c7689cee2af0.jpg', NULL, '2018-10-13 03:45:59', '2018-10-15 19:26:19', 'Array', NULL, NULL),
(41, 6, '1679091c5a880faf6fb5e6087eb1b2dc_c2b0339550f14188792592cb9ec79263.jpg', NULL, '2018-10-15 19:27:38', '2018-10-15 20:16:33', 'Array', NULL, NULL),
(42, 6, '1679091c5a880faf6fb5e6087eb1b2dc_ead3aadcddfd3efd209e6e02d0b62057.jpg', NULL, '2018-10-15 19:28:08', '2018-10-15 20:16:38', 'Array', NULL, NULL),
(44, 6, '1679091c5a880faf6fb5e6087eb1b2dc_6b4e9008fee3569a52f254885be100ec.jpg', NULL, '2018-10-15 19:29:12', '2018-10-15 20:16:36', 'Array', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message_email` varchar(256) DEFAULT NULL,
  `message_subject` varchar(256) DEFAULT NULL,
  `message_content` text,
  `message_created` datetime NOT NULL,
  `message_sent` datetime DEFAULT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`message_id`, `p_id`, `user_id`, `message_email`, `message_subject`, `message_content`, `message_created`, `message_sent`) VALUES
(9, 12, 6, '"vzhulkovsky@gmail.com"', NULL, '"sdfsdfsdfsdf"', '2018-10-15 21:59:21', '2018-10-15 22:01:21');

-- --------------------------------------------------------

--
-- Table structure for table `postcards`
--

CREATE TABLE IF NOT EXISTS `postcards` (
  `p_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `img_id` int(11) NOT NULL,
  `p_text` text NOT NULL,
  `p_created` datetime NOT NULL,
  `p_modified` datetime NOT NULL,
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `postcards`
--

INSERT INTO `postcards` (`p_id`, `user_id`, `img_id`, `p_text`, `p_created`, `p_modified`) VALUES
(2, 6, 36, '{"postcard-text-coord_0":"33,57","postcard-text_0":"Headline%20at%20the%20top%20of%20the%20image,%202%20lines%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A","postcard-text-width_0":"593","postcard-text-height_0":"128","postcard-image-width_0":"1075","postcard-image-height_0":"645","postcard-text-size_0":"70","postcard-text-coord_1":"461,48","postcard-text_1":"More%20text%20on%20the%20bottom%20of%20the%20image%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A","postcard-text-width_1":"852","postcard-text-height_1":"54","postcard-image-width_1":"1075","postcard-image-height_1":"645","postcard-text-size_1":"47"}', '2018-10-13 17:40:12', '2018-10-16 03:27:17'),
(6, 6, 39, '{"postcard-text-coord_0":"87,134","postcard-text_0":"Vail%20Colorado%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A","postcard-text-width_0":"596","postcard-text-height_0":"100","postcard-image-width_0":"1073","postcard-image-height_0":"644","postcard-text-size_0":"70","postcard-text-coord_1":"216,196","postcard-text_1":"Postcard%20body%20copy%20goes%20here%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A","postcard-text-width_1":"596","postcard-text-height_1":"100","postcard-image-width_1":"1073","postcard-image-height_1":"644","postcard-text-size_1":"47"}', '2018-10-13 20:32:00', '2018-10-15 01:54:27'),
(11, 6, 42, '{"postcard-text-coord_0":"265,576","postcard-text_0":"In%20a%20small%20Box%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A","postcard-text-width_0":"330","postcard-text-height_0":"148","postcard-image-width_0":"1075","postcard-image-height_0":"645","postcard-text-size_0":"70","postcard-text-coord_1":"417,575","postcard-text_1":"Postcard%20body,%20click%20to%20edit%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A%0A%20%20%20%20%20%20%20%20%0A%0A%0A%0A%0A%0A%0A%0A","postcard-text-width_1":"333","postcard-text-height_1":"143","postcard-image-width_1":"1075","postcard-image-height_1":"645","postcard-text-size_1":"47"}', '2018-10-15 20:30:36', '2018-10-16 03:30:47');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
