Postcard Creator Project

Web application that takes image input from the user (drag and drop or file upload).
Modifies the image, and sends it as an email to a specified recipient.


## Languages Used

- HTML / CSS
- JavaScript
- PHP

## Requirements

- Linux or OSX
- PHP7
- MySQL
- Browsers: Chrome, FF, Safari

## Source Code

git clone https://zhulko@bitbucket.org/zhulko/postcard.git


## Installation

Optionally, if you do not use Vagrant:

- Install VirtualBox - https://www.virtualbox.org/wiki/Downloads
- Install Vagrant - https://www.vagrantup.com/downloads.html

If you are using Vagrant:

- From source files, please copy `Vagrantfile` and `bootstrap.sh` files from `_vagrant` folder and place them inside a folder on your machine.
- In terminal `cd` to this folder and from there do `vagrant box add ubuntu/trusty64`
- Then do `vagrant up` to run the box
- Visit `192.168.33.66` in a browser to view the project


## Notes

- MySQL root password and the PHPMyAdmin root password are set to `12345678`
- Project is installed in `/var/www/html/postcard` 


## Manual Installation

1. Edit the database credentials in `application/config/config.php`
2. Execute the .sql statements in the `_install/`-folder (with PHPMyAdmin for example).
3. Make sure you have mod_rewrite activated on your server / in your environment. Some guidelines:
4. Install composer and run `composer install` in the project's folder to create the PSR-4 autoloading stuff from Composer automatically.
   


## Server configs for

### nginx

```nginx
server {
    server_name default_server _;   # Listen to any servername
    listen      [::]:80;
    listen      80;

    root /var/www/html/postcard/public;

    location / {
        index index.php;
        try_files /$uri /$uri/ /index.php?url=$uri&$args;
    }

    location ~ \.(php)$ {
        fastcgi_pass   unix:/var/run/php5-fpm.sock;
        fastcgi_index  index.php;
        fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}
```


## External libraries and tools 

**virtualbox**

- Vitrual Machine, needed to run Vagrant
- 5.2
- GPL
- https://www.virtualbox.org

**Vagrant**

- A tool for fast setup of a virtual environment
- 2.1.5
- MIT License
- https://www.vagrantup.com

**MINI3**

- An simple skeleton PHP application for faster setup
- August 2016
- MIT License
- https://github.com/panique/mini3

**Encoding**

- Helper class to encode text
- Version 2.0.
- BSD
- https://github.com/neitanod/forceutf8

**PHPMailer**

- PHP email creation and transport class.
- Version 5.5.
- GNU
- https://github.com/PHPMailer/PHPMailer

**Class upload**

- Image upload helper class
- 0.34
- GNU Public License
- https://www.verot.net/php_class_upload.htm


## TESTING

Database initialization command from `/var/www/html/postcard/Tests/Database`

`php scripts/install.php env=unittesting dropTables=true`

*unfinished phpunit*

Run from `/var/www/html/postcard/Tests` as

`phpunit`


