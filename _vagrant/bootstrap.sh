#!/usr/bin/env bash

# If you want to use a custom password for your database, then change it here, this script will install MySQL /
# phpmyadmin with that password and also put this into MINI3's config file.
# Use single quotes instead of double quotes to make it work with special-character passwords

PASSWORD='12345678'
PROJECTFOLDER='postcard'

sudo apt-get update
sudo apt-get -y upgrade

sudo apt-get install -y zip
sudo apt-get install -y unzip

sudo apt-get install -y apache2
sudo apt-get install -y php
sudo apt-get install -y phpunit

sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $PASSWORD"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $PASSWORD"
sudo apt-get -y install mysql-server
sudo apt-get install php-mysql

sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password $PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password $PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2"
sudo apt-get -y install phpmyadmin

# Create project folder, written in 3 single mkdir-statements to make sure this runs everywhere without problems
sudo mkdir "/var/www"
sudo mkdir "/var/www/html"
sudo mkdir "/var/www/html/${PROJECTFOLDER}"

# setup hosts file
echo "## setup hosts file"
VHOST=$(cat <<EOF
<VirtualHost *:80>
    DocumentRoot "/var/www/html/${PROJECTFOLDER}/public"
    <Directory "/var/www/html/${PROJECTFOLDER}/public">
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF
)
echo "${VHOST}" > /etc/apache2/sites-available/000-default.conf

# enable mod_rewrite
echo "## enable mod_rewrite"
sudo a2enmod rewrite

# adjust upload size
sudo sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 25M/" "/etc/php/7.2/apache2/php.ini"
sudo sed -i "s/post_max_size = 8M/post_max_size = 30M/" "/etc/php/7.2/apache2/php.ini"

# install sendmail
echo "### install sendmail"
sudo apt-get -y install sendmail

# remove default apache index.html
echo "## remove default apache index.html"
sudo rm "/var/www/html/index.html"

# adjust hosts for sendmail
echo "## adjust hosts and .conf files"
sudo sed -i "s/localhost/localhost vagrant-ubuntu-trusty-64/" "/etc/hosts"
sudo sed -i '$a HostsFile=/etc/hosts' /etc/mail/sendmail.conf
sudo sed -i '$a ServerName localhost' /etc/apache2/apache2.conf
##sudo sed -i '$a 192.168.33.66 postcard.creator' /etc/hosts

# install git
echo "## install git"
sudo apt-get -y install git

# git clone Postcard
sudo git clone https://zhulko@bitbucket.org/zhulko/postcard.git "/var/www/html/${PROJECTFOLDER}"

# install Composer
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

# go to project folder, create the PSR4 autoloader with Composer
cd "/var/www/html/${PROJECTFOLDER}"
composer install

# run SQL statements from Postcard folder
sudo mysql -h "localhost" -u "root" "-p${PASSWORD}" < "/var/www/html/${PROJECTFOLDER}/_install/pc_app.sql"

# put the password into the application's config. This is quite hardcore, but why not :)
sudo sed -i "s/12345678/${PASSWORD}/" "/var/www/html/${PROJECTFOLDER}/application/config/config.php"

# restart apache
echo "## 1 restart apache"
service apache2 restart

# final feedback
echo "Voila!"
